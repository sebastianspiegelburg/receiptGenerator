DROP SCHEMA testDatabase;
CREATE DATABASE IF NOT EXISTS testDatabase;

USE testDatabase;

CREATE TABLE Product
(
IdNumber int(10) NOT NULL CHECK(IdNumber > 0),
name varchar(20) NOT NULL ,
buyPrice float(10,2) NOT NULL,
sellPrice float(10,2) NOT NULL,
weight float(6,4) CHECK(weight> 0),
volume  float(6,4) CHECK(volume > 0),
CONSTRAINT PK_Pr_IdNumber PRIMARY KEY ( IdNumber )  
);
CREATE TABLE Receipt
(
IdNumber int(10) NOT NULL CHECK(IdNumber> 0),
date DATE NOT NULL,
CONSTRAINT PK_Re_IdNumber PRIMARY KEY ( IdNumber )  
);
CREATE TABLE Orders
(
ProIdNumber int(10) NOT NULL ,
RecIdNumber int(10) NOT NULL ,
soldUnits int(6) NOT NULL CHECK(soldUnits > 0),
CONSTRAINT FK_ProIdNumber FOREIGN KEY ( ProIdNumber ) REFERENCES Product( IdNumber ),
CONSTRAINT FK_RecIdNumber FOREIGN KEY ( RecIdNumber ) REFERENCES Receipt( IdNumber ) 
);

INSERT INTO Product
( IdNumber, name, buyPrice, sellPrice, volume,  weight )
VALUES
(1, 'erctzmns', 35.07, 42.79, 55.575, 0.373 ),
(2, 'fyoagzr', 27.97, 34.12, 70.018, 0.884 ),
(3, 'krymafohng', 48.97, 54.85, 7.301, 0.072 ),
(4, 'nhpsltx', 26.31, 36.31, 42.212, 0.05 ),
(5, 'ovj', 1.76, 2.41, 42.019, 0.622 ),
(6, 'qvmgbl', 2.93, 3.37, 32.205, 0.509 ),
(7, 'uhrkabzpt', 35.72, 56.79, 98.591, 0.399 ),
(8, 'usxkj', 6.78, 9.49, 35.593, 0.438 ),
(9, 'hck', 41.68, 55.85, 39.101, 0.518 ),
(10, 'uytweidphc', 47.8, 71.7, 92.227, 0.094 ),
(11, 'xbeht', 38.11, 56.78, 58.165, 0.118 ),
(12, 'umviewgxr', 2.4, 3.5, 55.234, 0.622 ),
(13, 'vuhtmnfc', 2.99, 3.32, 86.678, 0.475 ),
(14, 'uehjfxs', 11.54, 14.77, 43.057, 0.691 ),
(15, 'liqafzrx', 9.36, 12.64, 59.888, 0.814 ),
(16, 'trng', 35.23, 55.66, 85.31, 0.798 ),
(17, 'cjfqwns', 38.81, 44.24, 52.815, 0.383 ),
(18, 'grqcilwday', 39.15, 59.9, 22.614, 0.118 ),
(19, 'xcodm', 42.08, 58.91, 23.161, 0.14 ),
(20, 'gpsbk', 1.06, 1.67, 83.967, 0.831 ),
(21, 'ontrwskvh', 49.53, 71.82, 49.881, 0.631 ),
(22, 'wrkoevsfml', 43.19, 52.26, 73.895, 0.84 ),
(23, 'xgrm', 5.89, 7.01, 78.847, 0.64 ),
(24, 'zvwgdonhf', 58.5, 71.37, 1.394, 0.251 ),
(25, 'vrjeqtkgx', 15.7, 19.0, 4.551, 0.757 ),
(26, 'sfqioldtez', 51.0, 70.38, 26.861, 0.74 ),
(27, 'njptvogdys', 58.7, 64.57, 84.825, 0.96 ),
(28, 'oaqsjvh', 10.27, 11.91, 2.879, 0.994 ),
(29, 'gcqytnrfe', 14.41, 22.05, 70.367, 0.001 ),
(30, 'nvt', 20.91, 31.57, 79.898, 0.607 );
INSERT INTO Receipt
( IdNumber, date )
VALUES
(1, '2015-05-06'),
(2, '2017-06-07'),
(3, '2015-02-17'),
(4, '2015-02-09'),
(5, '2017-01-31');
INSERT INTO Orders
( ProIdNumber, RecIdNumber, soldUnits )
VALUES
(3, 1, 12),
(23, 1, 20),
(5, 1, 1),
(29, 2, 10),
(12, 2, 20),
(26, 2, 13),
(18, 2, 16),
(21, 2, 18),
(19, 2, 14),
(2, 2, 5),
(22, 3, 2),
(26, 3, 12),
(16, 3, 1),
(25, 3, 18),
(2, 3, 11),
(5, 3, 19),
(23, 3, 16),
(30, 3, 11),
(16, 4, 17),
(22, 4, 12),
(7, 4, 2),
(11, 4, 17),
(3, 4, 17),
(19, 4, 10),
(28, 4, 12),
(6, 4, 19),
(28, 5, 11),
(14, 5, 2),
(23, 5, 10),
(26, 5, 19),
(6, 5, 11),
(19, 5, 20),
(9, 5, 20),
(29, 5, 13),
(3, 5, 11),
(27, 5, 16);