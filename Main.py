
'''
Created on Feb 15, 2018

This File Controles the use of the program. The following struture is build to make  an SQL-File filled with Data struture and 
data content.

@author: Sebastian Spiegelburg
'''

from receiptGenerator.generateItems import *
from receiptGenerator.generateReceipt import *
from receiptGenerator.makeSql import *
from receiptGenerator.makeDML import *
from receiptGenerator.makeDDL import *

from test.testGenerator import *

if __name__ == '__main__':
    
    amountOfReceiptsToCreate = 100
    amountOfProductsToCreate = 500

    myProductList  = ItemList(amountOfProductsToCreate)
    myRecieptList = ReceiptList(amountOfReceiptsToCreate,myProductList)
    ddlobject = makeDDL()
    dmlobject = makeDML(myProductList,myRecieptList)

    #myProductList.printListContent()
    #myRecieptList.printListContent()
    makeSQLFile(ddlobject,dmlobject)

