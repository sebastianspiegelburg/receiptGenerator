The purpose of this project
=============================
 
 
 To make an interface between data in python variables and MySQL Database.
 We can pursue this goal by creating a data structure in MySQL also call DDL.
 and the fill the data into this structure. You can see my chosen ER-Chart in the
 “doc” folder. The final product is a created fill that can directly be imported 
into a MySQL DBMS.
 
 As a side project i generated fictional data in form of products and receipts.
 This data has many parameter to customize it to make in more realistic. 
  
-----------------------------
How to Use it
-----------------------------

in the File "Main.py" you can change the amount of receipts and product that
will get created. To change any of the default setting you can edit the 
globals variables of the classes. Also you can uncomment the “printList” to see 
the content of the object in a orderly fashion in the comandline output.

If you just wish to see an example file. Look into ./result/fictionalDatabase.sql.
There is working example that you can import into MySQL 5.7

Please make sure to read the class description to know how to use them.

To secure the correct working of the project I wrote test cases for all
 major steps  into "./test/testGenerator.py". You can use the program 
 "nosetests" to check the proper working of the project.

-------------------------------
Additions to follow
-------------------------------
To make the receipts more realistic i will add additional function where
you can defined more precisely which product have higher probability to 
be sold and which haven't.
