'''
Created on Feb 15, 2018

@author: Sebastian Spiegelburg
Generate a list of products and receiced with randomly generated
content in it. The resulting file will be in SQL Format and can be
used to importe it in MySQL Database table.

Personal Note:
Issue with multiple String with same name is not important because
all the the products get a identification number

'''

class makeSQLFile:
    '''
    Creates a finishied fill with all the nessesary information to get import
    into sql DBMS.
    '''
    pathToFile = "./result/"
    suffixSQLFile = ".sql"
    fileName = "fictionalDatabase"
    atLeastBufferSize = 3000
    

    def __init__(self,ddlObject,dmlObject):
        self.fileAccess = makeSQLFile.pathToFile + makeSQLFile.fileName \
                          + makeSQLFile.suffixSQLFile
        self.myFile = self.initaliseFile(self.fileAccess)
        sqlString =  ddlObject.getContent()
        sqlString += dmlObject.getContent()
        self.generateFile(sqlString)


    def generateFile(self,content):
        self.myFile.write(content)
        self.myFile.close()

    def initaliseFile(self,fileName):
        from io import DEFAULT_BUFFER_SIZE
        if DEFAULT_BUFFER_SIZE > makeSQLFile.atLeastBufferSize:
            bufferSize = DEFAULT_BUFFER_SIZE
        else:
            bufferSize = makeSQLFile.atLeastBufferSize

        myfile = open(fileName, "w", bufferSize, "utf-8")
        return myfile

import re

class makeDDL:

    '''
    Makes the structure of the tabels and adds all the nessesary 
    constraints to the  tables
    '''

    class labreg:

        databaseName = "testDatabase"
        TableLableProduct =  "Product"
        TableLableReceipt = "Receipt"
        TableLableOrder = "Orders" # Can't choice order (KEY WORD ISSUE MYSQL)
        idNumberAttr = "IdNumber"
        idNumberAttrforeign = "ProIdNumber"

        productNameAttr = "name"
        productBuyPriceAttr ="buyPrice"
        productSellPriceAttr = "sellPrice"
        productWeightAttr = "weight"
        productVolumeAttr = "volume"
        orderSoldUnitsAttr = "soldUnits"
        receiptSellDate = "date"
        TableLableProduct =  "Product"



    def __init__(self):

        self.begR = 0
        self.endR = 3
        self.__genContent = ""
        self.__genContent += self.makeRemoveDatabase()
        self.__genContent += self.makeDatabase()
        self.__genContent += self.makeAccessDatabase()
        self.__genContent += self.makeProductTable()
        self.__genContent += self.makeOrderTable()
        self.__genContent += self.makeReceiptTable()

    def show(self):
        print(self.getContent())

    def getContent(self):
        return self.__genContent

    def makeDatabase(self):
        return  "CREATE DATABASE IF NOT EXISTS " + makeDDL.labreg.databaseName + ";\n\n"
    def makeAccessDatabase(self):
        return "USE " + makeDDL.labreg.databaseName + ";\n\n"
    def makeRemoveDatabase(self):
        return "DROP SCHEMA " + makeDDL.labreg.databaseName + ";\n"

    def makeProductTable(self):
        lr = makeDDL.labreg
        attrList = [
            lr.idNumberAttr+ " int(10) NOT NULL CHECK(" + lr.idNumberAttr + " > 0)"  ,
            lr.productNameAttr + " varchar(20) NOT NULL ",
            lr.productBuyPriceAttr + " int(6) NOT NULL",
            lr.productSellPriceAttr + " int(6) NOT NULL",
            lr.productWeightAttr + " int(5) CHECK(" + lr.productWeightAttr + "> 0)",
            lr.productVolumeAttr + "  int(5) CHECK(" + lr.productVolumeAttr + " > 0)" ]
        constraintList =  [self.addConstraintPrimaryKey(lr.idNumberAttr)]
        return self.addContentTable(lr.TableLableProduct, attrList, constraintList)


    def makeOrderTable(self):
        lr = makeDDL.labreg
        fKey = lr.idNumberAttrforeign
        attrList = [fKey + " int(10) NOT NULL " ,
            lr.orderSoldUnitsAttr + " int(6) NOT NULL CHECK("+ lr.orderSoldUnitsAttr + " > 0)"]
        constraintList = [self.addConstraintUniqueKey(fKey)]
        constraintList += [self.addConstraintForeignKey(fKey,lr.TableLableProduct,lr.idNumberAttr)]
        return self.addContentTable(lr.TableLableOrder, attrList, constraintList)

    def makeReceiptTable(self):
        lr = makeDDL.labreg
        fKey = lr.TableLableOrder[:3] + lr.idNumberAttrforeign
        attrList = [fKey + " int(10) NOT NULL ",
                    lr.idNumberAttr + " int(10) NOT NULL CHECK(" + lr.idNumberAttr + "> 0)",
                    lr.receiptSellDate + " DATE NOT NULL"]
        constraintList =  [self.addConstraintForeignKey(fKey,lr.TableLableOrder,fKey[3:])]
        return self.addContentTable(lr.TableLableReceipt, attrList, constraintList)



    def addConstraintUniqueKey(self,attriName):
        return "CONSTRAINT UK_" + attriName  +" UNIQUE KEY (" + attriName + ")"

    def addConstraintPrimaryKey(self,keyName):
        return "CONSTRAINT PK_" + keyName + " PRIMARY KEY ( " + keyName + " ) "


    def addConstraintForeignKey(self,keyNameThisTabel,foreignTable,fTKeyName):
        return "CONSTRAINT FK_" + keyNameThisTabel + " FOREIGN KEY ( " + keyNameThisTabel + " ) "  + \
            "REFERENCES " + foreignTable + "( " + fTKeyName + " )"

    def addContentTable(self,tableName,attributeList,constraintList):
        makeTable = "CREATE TABLE "  + tableName
        finalList = attributeList  + constraintList
        makeTable += self.addAttributSyntax(finalList)
        return makeTable

    def addAttributSyntax(self,aList):
        attributString = "\n(\n"
        for i in aList:
            attributString += i + ",\n"
        attributString = attributString[:-2]
        attributString += " \n);\n" 
        return attributString
    
class makeDML:
    '''
    Fills the data into the table above
    '''
    def __init__(self,itemListObject,receiptObject):


        self.fillproductTable(itemListObject)
        self.fillorderTable(receiptObject)
        self.fillreceiptTable(receiptObject)


    def setContent(self,value):
        self.__completedString

    def getContent(self):
        return self.__completedString


    def fillproductTable(self,itemListobject):
        lr = makeDDL.labreg
        insertComand = "INSERT INTO " + lr.TableLableProduct + "\n"
        insertComand += "( "
        insertComand += lr.idNumberAttr + ", " +lr.productNameAttr + ", " + \
                        lr.productBuyPriceAttr + ", " + lr.productSellPriceAttr + ", " + \
                        lr.productVolumeAttr + ",  " + lr.productWeightAttr
        insertComand += " )\n"
        insertComand += "VALUES\n"

        values = ""
        for aVal in itemListobject.getList():
            values += "("
            values += str(aVal.itemIdNumber) +  ", '" + str(aVal.ItemName) + "', "
            values += str(aVal.buyprice) + ", " + str(aVal.sellprice) + ", "
            values += str(aVal.weight) + ", " + str(aVal.volume)
            values += " ),\n"

        values = values[:-2]
        insertComand += values + ";"
        self.setContent(insertComand)

    '''
    INSERT INTO Product  (idNumber , name , buyPrice ,sellPrice, volume, weight) 
    VALUES(10,'sebastian',10,20,1000,10);

    INSERT INTO Product  
    VALUES(17,'sebastian',10,20,1000,10),
      (19,'sebastian',10,20,1000,10);
    
    '''
    def fillorderTable(self,receiptObject):
        lr = makeDDL.labreg
        insertComand = "INSERT INTO " + lr.TableLableOrder + "\n"
        insertComand += "( " + lr.idNumberAttrforeign + ", " +lr.orderSoldUnitsAttr + " )\n"
        insertComand += "VALUES\n"

        values = ""
        for aVal in itemListobject.getList():
            values += "("
            values += str(aVal.itemIdNumber) +  ", '" + str(aVal.ItemName) + "', "
            values += str(aVal.buyprice) + ", " + str(aVal.sellprice) + ", "
            values += str(aVal.weight) + ", " + str(aVal.volume)
            values += " ),\n"

        values = values[:-2]
        insertComand += values + ";"
        return insertComand

    def fillreceiptTable(self,productList):
        pass

