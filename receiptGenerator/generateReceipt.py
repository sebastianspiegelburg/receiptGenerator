
'''
Created on Feb 17, 2018

@author: Sebastian Spiegelburg

In this module we generate receipts. Each receip gets random amount of different product that gets different amount of purches

'''

import random as  rd
import datetime
import time

class ReceiptList():

    def __init__(self, amountToGenerate,listOfProducts):
        self.allRecieptList = []
        for i in range(amountToGenerate):
            self.allRecieptList.append(Receipt(listOfProducts))

    def printListContent(self):
        for reciept in self.allRecieptList:
            reciept.printContent()
    

       # Choice  a number of Products you want to buy 
       # Choice a Random Product
       # Choice a Random amount that you buy  that product
       # small numbers should occure much more than big

       # make a tuble  for (products and amountsold)
       # make a tuple for ( receiptID, SumSold, [ LIst of Tuples] 
       
       # calculate Sum

class Receipt:
    '''
    this class makes a receipt we can have inpact on the generated receipt with the following variables 
    the constructor takes a List with all items that are a vailable to purchas.
    '''

    maxAmountOfDifferenProductsInReceipt = 20
    maxAmountOfSoldUnitesSameType = 10
    receiptIDnumber = 0
    beginTime = "1-1-2015"
    endTime = "20-11-2017"
    
    
    def fromStringToTimeObject(self,aTime):
        dataList = aTime.split('-')
        print(dataList)
        myTime = datetime.datetime(int(dataList[2]),int(dataList[1]),int(dataList[0]))
        return time.mktime(myTime.timetuple())
    
    def fromTimeobjectToString(self,aTimestamp):
        value = datetime.datetime.fromtimestamp(aTimestamp)
        return value.strftime('%d-%m-%Y')
    
    def randomDateGenerate(self,begin,end):
        beginValue = self.fromStringToTimeObject(begin)
        endValue = self.fromStringToTimeObject(end)
        betweenValue = rd.uniform(beginValue,endValue)
        return self.fromTimeobjectToString(betweenValue)
    
    '''  Generates  a Tuple in the following order ( receiptId, totaleAmount,[(ProductId,AmountSod)....])''' 
    def __init__(self, aListWithItems):
        self.generatedTupleLIst = self.generateTuple(aListWithItems)


    def generateTuple(self,aListWithItems):
        self.receiptid = Receipt.generateIDNumber()
        self.productSoldTuple = Receipt.getTuple_ProductID_SoldUniteAmout(aListWithItems)
        self.sumSold = Receipt.sumofSoldProducts(self.productSoldTuple,aListWithItems)
        self.aChoosenDate = self.randomDateGenerate(Receipt.beginTime, Receipt.endTime)
        return (self.receiptid,self.sumSold,self.productSoldTuple,self.aChoosenDate)

    '''
    Returns a Tuple  with two Elements the first the Product identication number the second how many units have been sold.
    '''
    def getTuple_ProductID_SoldUniteAmout(listPossibleProducts):
         
        listWithTuples =  []
        amountOfDifferentArticles = Receipt.randomAmountOfDifferenSoldProducts()
        listWithDifferentRandomArticle = rd.sample(listPossibleProducts.itemList,amountOfDifferentArticles)

        for i in range(amountOfDifferentArticles):
            anAmountOfSoldUnites = Receipt.randomAmountOfSoldSameProduct()
            aProduct = listWithDifferentRandomArticle[i].itemIdNumber
            aTuple = (aProduct, anAmountOfSoldUnites)
            listWithTuples.append(aTuple)
            
        return listWithTuples



    def sumofSoldProducts(productList,allPossibleProducts):
        finalSume = 0
        for tuple in productList:
            productidNumber = tuple[0]
            timesSold = tuple[1]

            product = allPossibleProducts.itemList[productidNumber- 1]
            valueOfProduct = product.sellprice
            finalSume += valueOfProduct * timesSold
        
        return round(finalSume,2)

            
    
    def randomAmountOfSoldSameProduct():
        return rd.randint(1,Receipt.maxAmountOfDifferenProductsInReceipt)
    

    def randomAmountOfDifferenSoldProducts():
        return rd.randint(1,Receipt.maxAmountOfSoldUnitesSameType)

    def generateIDNumber():
        Receipt.receiptIDnumber+= 1
        return Receipt.receiptIDnumber

    def printContent(self):
        print(self.receiptid,self.sumSold,self.productSoldTuple,self.aChoosenDate)