'''
Created on Feb 15, 2018

@author: Sebastian Spiegelburg

The File will Return a List of Prodcucts. Each of them will have the follwoing attributes;
product name, buy value, sell value,weight value and volume  value.This generate will fill the fields with arbitrary values that
can be configured after certain condition. They all are in the item Class. The item list class will use the item class to generate
many items and will put them into a list.
'''

import random as rd
import string
    
class ItemList:
    '''
    Generates a list of items with the class beneath only variable that needs specifications of the amount of items.
    '''
    def __init__(self,amountItems):
        self.amountItems = amountItems
        self.itemList = []
        for i in range(amountItems ):
            self.itemList.append(Item())
    def getList(self):
        return self.itemList
    def getListsize(self):
        return self.amountItems
        
    def choiceAnItemRandomly(self):
        return rd.choice(self.itemList)
            
    def printListContent(self ):
        for item in self.itemList:
            item.printContent()


class Item:

    amountOfProducts = 0
    minAmountofLetters = 3
    maxAmountofLetters = 10
    minProfitPercent = 10
    maxProfitPercent = 60
    maxPriceProduct = 1500
    
    minWeight = 0.005
    maxWeight = 100
    minVolume = 0.001
    maxVolume = 1
    
    precisionWeightDigit = 3
    precisionVolumDigit = 3

    def __init__(self):
        self.itemIdNumber = Item.amountOfProducts
        Item.generateProductIdnumber()
        self.ItemName = self.generateRandomString(self.minAmountofLetters,self. maxAmountofLetters)
        buyAndSell = self.generateRandomBuyInandSellValue(self.minProfitPercent,self.maxProfitPercent,self.maxPriceProduct)
        self.buyprice = buyAndSell[0]
        self.sellprice = buyAndSell[1]
        self.weight = self.generateProductWeight(self.minWeight,self.maxWeight)
        self.volume = self.generateProductVolume(self.minVolume,self.maxVolume)




    '''Makes a String with only small letters in a with max amount of maxSize and at least minSize'''
    def generateRandomString(self,minSize,maxSize):
        letter_a = 97
        smallAlphabeticLetters = [chr(i) for i in range(letter_a,letter_a + 26)]
        wordSize = rd.randint(minSize,maxSize)
        wordList = rd.sample(smallAlphabeticLetters,wordSize)
        return "" .join(wordList)
        

    '''Making a random buy value and a random sell value depending on the possible wining range 
        and the possible buy in range '''
    def generateRandomBuyInandSellValue(self,minProfitPercent,maxProfitPercent,maxPriceProductBuyIn):
        buyInPrice = round(rd.uniform(1,maxPriceProductBuyIn),2)
        randomPercent = (rd.randint(minProfitPercent,maxProfitPercent) / 100) + 1
        sellPrice = round(randomPercent * buyInPrice,2)
        return  [buyInPrice,sellPrice]

    ''' Makes for each product an increasing product number '''
    def generateProductIdnumber():
        Item.amountOfProducts  = 1 + Item.amountOfProducts
        #return Item.amountOfProducts
        
        
    ''' Makes a random Volume in cubic meter depending on minVolumen and maxVolume, precision to the 3 digit '''
    def generateProductVolume(self,minVolumen,maxVolumen):
        return self.generateRandomNumberWithCertainPrecision(minVolumen, maxVolumen, self.precisionWeightDigit)

    ''' Make a random weight in Kg depending on the minWeight and maxWeight '''
    def generateProductWeight(self,minWeight,maxWeight):
        return self.generateRandomNumberWithCertainPrecision(minWeight, maxWeight, self.precisionVolumDigit)    
        
    '''   Generates a random float number  with a certain precision '''
    def generateRandomNumberWithCertainPrecision(self,min,max,precision):
        return round(rd.uniform(min,max),precision)
        
    
    '''  Human Readable Print of an item with all its qualities '''    
    def printContent(self):
        print (self.itemIdNumber, self.ItemName, self.buyprice, self.sellprice, self.weight, self.volume)
