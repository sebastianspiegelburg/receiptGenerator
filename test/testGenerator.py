
'''
Created on Feb 15, 2018

@author: Sebastian Spiegelburg

The whole project gets tested in one testfile because the project is very small.


issues:

    def test_Method_generateRandomString(self,testSize=10) has a random generateor so
    test can possible be not consistent. With many times of testing the chancecs should get lower
    of an error.
'''

import unittest
import os
import io
import sys
import datetime,time
from pathlib import Path


from receiptGenerator import generateReceipt
from receiptGenerator import generateItems




class TestReceiptPackage(unittest.TestCase):

    '''
    Tests for "generateItems"
    '''
    testItem = generateItems.Item() 


    def test_template(self):
        pass

    def test_Method_generateRandomString(self,testSize=10):
        # Setting Up Test with deactivating Random
        import  random as rd
        def randintMockUp(aNumber,aNumber2):
            return self.testNumber

        randintOriginale = rd.randint
        rd.randint = randintMockUp
        
        lowEndSampleSize = 4
        highEndSampleSize = 10  
        for i in range(lowEndSampleSize,highEndSampleSize+ 1):
            self.testNumber = i
            checkString = self.testItem.generateRandomString(lowEndSampleSize,highEndSampleSize)
            self.assertTrue(len(checkString) >= lowEndSampleSize)
            self.assertTrue(len(checkString) <= highEndSampleSize)
            self.assertTrue(checkString.isalpha())
            
        #Restoring Originale Function
        rd.randint = randintOriginale
            
            
    def test_Methode_generateRandomBuyInandSellValue(self):
        # Setting Up Test with deactivating Random
        import  random as rd
        def randintMockUp(aNumber,aNumber2):
            return self.testNumberRandInt
        def uniformMockUp(aNumber,aNumber2):
            return self.testNumberUniform

        uniformOriginal = rd.uniform
        randintOriginale = rd.randint

        rd.randint = randintMockUp
        rd.uniform= uniformMockUp

        testResultBuyIn = 2
        testResultSellPrice = 4
        self.testNumberUniform = testResultBuyIn
        self.testNumberRandInt = 100

        testuniformValue = 0.5
        
        dumyNumber = 0
        resultList =self.testItem.generateRandomBuyInandSellValue(dumyNumber,dumyNumber,dumyNumber)
        correctList = [testResultBuyIn,testResultSellPrice]

        self.assertEqual(resultList, correctList)
        # Restore Methods
        rd.uniform = uniformOriginal
        rd.randint = randintOriginale

    def test_Methode_generateProductIdnumber(self):
        #Reseting Value
        generateItems.Item.amountOfProducts = 0
        counter = generateItems.Item.amountOfProducts
        self.assertTrue(counter == 0)
        generateItems.Item.generateProductIdnumber()
        counter = generateItems.Item.amountOfProducts
        self.assertTrue(counter == 1)
        generateItems.Item.generateProductIdnumber()
        counter = generateItems.Item.amountOfProducts
        self.assertTrue(counter == 2)
    
        # for additional Test with global Variable
        generateItems.Item.amountOfProducts = 1
    
    '''
    Tests for "generateReceipt"
    '''
        
    def test_fromStringToTimeObject(self):
        firstTimeUse = "2-1-1970" 
        dayLater = "3-1-1970" 
        timeInSec = generateReceipt.Receipt.fromStringToTimeObject(self,firstTimeUse)
        timeInSec2 = generateReceipt.Receipt.fromStringToTimeObject(self,dayLater)

        timeInSecOfOneDay =  timeInSec2 - timeInSec
        timeInSecOfOneDayShould = 24 * 60 * 60
        self.assertEqual(timeInSecOfOneDay,timeInSecOfOneDayShould)
    def test_fromTimeobjectToString(self):
        chrismaseTime = datetime.datetime(2018,12,24)
        chrismaseTime = time.mktime(chrismaseTime.timetuple())
        resulteDate = generateReceipt.Receipt.fromTimeobjectToString(self,chrismaseTime)
        self.assertEqual("24-12-2018", resulteDate)

     
    def test_getTuple_ProductID_SoldUniteAmout(self):
        #setup 
        self.testNumber = 3
        def mockRS(a,b):
            return self.testItemList
        import random as  rd
        original_rs = rd.sample
        rd.sample = mockRS

        class fakeProductList:
            def __init__(self,iL):
                self.itemList = iL
        class fakeProduct:
            randomAmountOfDifferenSoldProducts = 3
            itemList = 3
            def randomAmountOfSoldSameProduct(self):
                return 1
            def __init__(self,number):
                self.itemIdNumber = number
        
        def fakeReturnValue():
            return self.testNumber - 1
        
        generateReceipt.Receipt.randomAmountOfDifferenSoldProducts = fakeReturnValue
        generateReceipt.Receipt.randomAmountOfSoldSameProduct = fakeReturnValue
            
        self.testItemList = []
        for i in range(self.testNumber):
            self.testItemList.append(fakeProduct(i))

        fPL = fakeProductList(self.testItemList)
        gR = generateReceipt.Receipt
        result = gR.getTuple_ProductID_SoldUniteAmout(fPL)


        #Recover 
        rs = original_rs

        self.assertEqual(result[0] , (0,self.testNumber -1 ))
        self.assertEqual(result[1] , (1,self.testNumber -1 ))
        
    def test_sumofSoldProducts(self):
        # Fake Tuple List
        
        # Fake Product List Class with Fake Product Class
        
        fakeTupleList = [(0,2),(1,3),(2,4)]
        class fakeProductList:
            class fakeProduct:
                sellprice = 2
                def __init__(self):
                    pass
                
            itemList = []
            def __init__(self,size):
                for i in range(size):
                    fakeProductList.itemList.append(fakeProductList.fakeProduct())

        result = generateReceipt.Receipt.sumofSoldProducts(fakeTupleList,fakeProductList(len(fakeTupleList)))
        self.assertEqual(result, 18)

    def test_generateIDNumber(self):
        self.assertEqual(generateReceipt.Receipt.receiptIDnumber,0)
        self.assertEqual(generateReceipt.Receipt.generateIDNumber(),1)
        self.assertEqual(generateReceipt.Receipt.receiptIDnumber,1)
    
    '''
    Tests for "makeSql"
    '''